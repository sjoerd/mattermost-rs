use serde::Serialize;
use serde_json::Value;
use std::collections::HashMap;
use thiserror::Error;

#[derive(Debug, Clone, Error)]
pub enum MessageBuilderError {
    #[error("No text specified")]
    MissingText,
}

#[derive(Debug, Default)]
pub struct MessageBuilder<'a> {
    text: Option<&'a str>,
    channel: Option<&'a str>,
    card: Option<&'a str>,
}

impl<'a> MessageBuilder<'a> {
    pub fn text(mut self, text: &'a str) -> Self {
        self.text = Some(text);
        self
    }

    pub fn channel(mut self, channel: &'a str) -> Self {
        self.channel = Some(channel);
        self
    }

    pub fn card(mut self, card: &'a str) -> Self {
        self.card = Some(card);
        self
    }

    pub fn build(self) -> Result<Message<'a>, MessageBuilderError> {
        let text = match self.text {
            Some(text) => text,
            None => return Err(MessageBuilderError::MissingText),
        };

        let props = match self.card {
            Some(card) => {
                let mut m = HashMap::new();
                m.insert("card", card.into());
                Some(m)
            }
            None => None,
        };
        let channel = self.channel;

        Ok(Message {
            text,
            props,
            channel,
        })
    }
}

#[derive(Clone, Debug, Serialize)]
pub struct Message<'a> {
    text: &'a str,
    #[serde(skip_serializing_if = "Option::is_none")]
    channel: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    props: Option<HashMap<&'a str, Value>>,
}

impl<'a> Message<'a> {
    pub fn new_text(text: &str) -> Message {
        Message {
            text,
            props: None,
            channel: None,
        }
    }
}
