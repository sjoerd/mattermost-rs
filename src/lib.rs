pub mod message;

use log::{debug, info};
use message::Message;
use reqwest::{Client, StatusCode};
use thiserror::Error;
use url::Url;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum MattermostError {
    #[error("Failed to post")]
    PostError(#[from] reqwest::Error),
    #[error("Not found (unknown channel?): {0}")]
    NotFound(reqwest::Error),
    #[error("Unknown error response: {0}")]
    UnknownResponse(reqwest::Error),
}

pub struct Mattermost {
    hook: Url,
    client: Client,
}

impl Mattermost {
    pub fn new(url: &str) -> Result<Mattermost, url::ParseError> {
        let hook: Url = url.parse()?;
        let client = reqwest::Client::new();
        Ok(Mattermost { hook, client })
    }

    pub async fn send_text(&self, text: &str) -> Result<(), MattermostError> {
        let m = Message::new_text(text);
        self.send(m).await
    }

    pub async fn send(&self, m: Message<'_>) -> Result<(), MattermostError> {
        info!("Sending: {}", serde_json::to_string(&m).unwrap());
        let r = self.client.post(self.hook.clone()).json(&m).send().await?;
        debug!("Response: {:#?}", r);
        match r.error_for_status() {
            Ok(_) => Ok(()),
            Err(e) if e.status() == Some(StatusCode::NOT_FOUND) => {
                Err(MattermostError::NotFound(e))
            }
            Err(e) => Err(MattermostError::UnknownResponse(e)),
        }
    }
}
