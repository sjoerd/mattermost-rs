use anyhow::{Context, Result};
use mattermost_rs::message::MessageBuilder;
use mattermost_rs::Mattermost;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbose: usize,
    #[structopt(short, long)]
    url: String,
    #[structopt(long)]
    card: Option<String>,
    #[structopt(long)]
    channel: Option<String>,
    text: String,
}

#[tokio::main]
pub async fn main() -> Result<()> {
    let opt = Opt::from_args();
    stderrlog::new().verbosity(opt.verbose).init()?;
    let mm = Mattermost::new(&opt.url).unwrap();

    let mut b = MessageBuilder::default().text(&opt.text);

    if let Some(card) = opt.card.as_ref() {
        b = b.card(card);
    }

    if let Some(channel) = opt.channel.as_ref() {
        b = b.channel(channel);
    }

    let msg = b.build().context("Couldn't build message")?;

    mm.send(msg).await?;
    Ok(())
}
